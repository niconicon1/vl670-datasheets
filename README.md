VL670 Development Board Datasheets - USB 2.0 to 3.0 Transaction Translator
========================================================================

These are the datasheets of the components in the VL670 development board.

This is a sub-repository of the VL670 development board, which is an
effort to create a free and open source hardware design of a development
board for the VL670/VL671 ASIC, to facilitate hardware evaluation and
experiments by other community developers.

This development board is intended solely for hardware or software
developers for use in a research and development setting to facilitate
evaluation, experimentation, or technical analysis. It's not designed
for consumer use, and it also should not be used as a part or subassembly
in any finished product.

The main repository is:

* https://notabug.org/niconiconi/vl670/

### List

* [VL670 USB2.0 to USB3.0 Transaction Translator](https://gitlab.com/niconicon1/vl670-datasheets/-/raw/main/DS_VL670_115.pdf)

* [VL671 Low Power USB 2.0 to USB 3.1 Gen1 Transaction Translator](https://gitlab.com/niconicon1/vl670-datasheets/-/raw/main/DS_VL671_080.PDF)

* [HD3SS3220 USB Type-C DRP Port Controller with SuperSpeed 2:1 MUX](https://gitlab.com/niconicon1/vl670-datasheets/-/raw/main/hd3ss3220.pdf)

* [TPS20xxC and TPS20xxC-2 Current Limited, Power-Distribution Switches](https://gitlab.com/niconicon1/vl670-datasheets/-/raw/main/tps2061c.pdf)

* [GD25D10C/05C 3.3V Uniform Sector Standard and Dual Serial Flash](https://gitlab.com/niconicon1/vl670-datasheets/-/raw/main/GD25D05C.pdf)

* [SN74LVC2G66 Dual Bilateral Analog Switch](https://gitlab.com/niconicon1/vl670-datasheets/-/raw/main/sn74lvc2g66.pdf)

* [ESD7104 Low Capacitance ESD Protection Diode for High Speed Data Line](https://gitlab.com/niconicon1/vl670-datasheets/-/raw/main/ESD7104-D.PDF)

* [ESD5B5.0ST1G-N ESD Protection Diode](https://gitlab.com/niconicon1/vl670-datasheets/-/raw/main/ESD5B5.0ST1G-N.pdf)

### License

All datasheets are copyrighted by their respective owners.
